$(function() {

	document.addEventListener("deviceready", onDeviceReady, false);

	function onDeviceReady() {
		// Affichage de la connexion
		var networkState = navigator.connection.type;

		var states = {};
		states[Connection.UNKNOWN]  = 'Connexion inconnue';
		states[Connection.ETHERNET] = 'Connexion Ethernet';
		states[Connection.WIFI]     = 'Connexion WiFi';
		states[Connection.CELL_2G]  = 'Réseau cellulaire 2G';
		states[Connection.CELL_3G]  = 'Réseau cellulaire 3G';
		states[Connection.CELL_4G]  = 'Réseau cellulaire 4G';
		states[Connection.CELL]     = 'Réseau cellulaire générique';
		states[Connection.NONE]     = 'Pas de connexion réseau';

		$(".container header h3").html(states[networkState]);
	}

	// Appareil photo
	$(".container button#camera").click(function() {
		$(".result .content h4").html("Appareil Photo");
		$(".result").show();

		function onSuccess(imageData) {
			$(".result .content div[data-plugin='camera'] img").attr("src","data:image/jpeg;base64,"+imageData);
			$(".result .content div[data-plugin='camera']").fadeIn();
		}

		function onError(error) {
			if (error !== "done") {
				navigator.notification.alert("Une erreur est survenue.\n"+error, null, "Erreur");
			}
		}
	
		navigator.camera.getPicture(onSuccess, onError, { 
			quality : 80,
			destinationType : Camera.DestinationType.DATA_URL,
			sourceType : Camera.PictureSourceType.CAMERA,
			allowEdit : false,
			encodingType: Camera.EncodingType.JPEG,
			correctOrientation: true,
			saveToPhotoAlbum: true
		});
	});

	// Géolocalisation
	$(".container button#location").click(function() {
		$(".result .content h4").html("Géolocalisation");
		$(".result").show();

		function onSuccess(position) {
			console.log(position);
			var result =	"Latitude: "		   + position.coords.latitude 	+ "<br />" +
							"Longitude: "		   + position.coords.longitude 	+ "<br />" +
							"Altitude: "		   + position.coords.altitude 	+ "<br />" +
							"Accuracy: "		   + position.coords.accuracy 	+ "<br />" +
							"Altitude Accuracy: " + position.coords.altitudeAccuracy + "<br />" +
							"Heading: "		 	   + position.coords.heading 	+ "<br />" +
							"Speed: "			   + position.coords.speed 		+ "<br />" +
							"Timestamp: "		   + position.timestamp 		+ "<br />";
			$(".result .content div[data-plugin='location']").fadeIn();
		};

		function onError(error) {
			navigator.notification.alert("Erreur de la géolocalisation\n"+error.message+"\nCode: "+error.code, null, "Erreur");
		};

		navigator.geolocation.getCurrentPosition(onSuccess, onError);
	});

	// Boussole
	var watchID_compass = null;
	$(".container button#compass").click(function() {
		$(".result .content h4").html("Boussole");
		$(".result").show();

		function onSuccess(heading) {
			$(".result .content div[data-plugin='compass'] p").html("Heading: "+Math.round(heading.magneticHeading*100)/100+"°");
		};

		function onError(error) {
			navigator.notification.alert("Erreur de la boussole: "+error.code, null, "Erreur");
		};

		watchID_compass = navigator.compass.watchHeading(onSuccess, onError, {
			frequency: 200 // 200ms
		});

		$(".result .content div[data-plugin='compass']").fadeIn();
	});

	// Appareil
	$(".container button#device").click(function() {
		$(".result .content h4").html("Appareil");
		$(".result").show();

		var result = 	"Device Name: "     + device.name     + "<br />" +
						"Device Model: "    + device.model    + "<br />" +
						"Device Cordova: "  + device.cordova  + "<br />" +
						"Device Platform: " + device.platform + "<br />" +
						"Device UUID: "     + device.uuid     + "<br />" +
						"Device Version: "  + device.version;

		$(".result .content div[data-plugin='device'] p").html(result);
		$(".result .content div[data-plugin='device']").fadeIn();
	});

	// Contacts
	$(".container button#contacts").click(function() {
		$(".result .content h4").html("Contacts");
		$(".result").show();
	});

	// Notification
	$(".container button#notification").click(function() {
		$(".result .content h4").html("Notification");
		$(".result").show();

		$(".result .content div[data-plugin='notification']").fadeIn();

		$(".result .content div[data-plugin='notification'] fieldset:first-child button").click(function() {
			var element = $(this).parent();
			var title = element.children("input[name='title']").val();
			var content = element.children("input[name='content']").val();
			navigator.notification.alert(content, null, title);
		});

		$(".result .content div[data-plugin='notification'] fieldset:nth-child(2) button").click(function() {
			var element = $(this).parent();
			var times = element.children("input[name='times']").val();
			navigator.notification.beep(times);
		});

		$(".result .content div[data-plugin='notification'] fieldset:nth-child(3) button").click(function() {
			var element = $(this).parent();
			var ms = element.children("input[name='ms']").val();
			navigator.notification.vibrate(ms);
		});

		$(".result .content div[data-plugin='notification'] fieldset input[type='range']").change(function() {
			$(this).parent().children("span").html($(this).val());
		});
	});

	// Accéléromètre
	var watchID_accelerometer = null;
	$(".container button#accelerometer").click(function() {
		$(".result .content h4").html("Accéléromètre");
		$(".result").show();

		function onSuccess(acceleration) {
			var result = 	'Acceleration X: ' + Math.round(acceleration.x*100)/100 + "<br />" +
							'Acceleration Y: ' + Math.round(acceleration.y*100)/100 + "<br />" +
							'Acceleration Z: ' + Math.round(acceleration.z*100)/100 + "<br />" +
							'Timestamp: '      + acceleration.timestamp;
			$(".result .content div[data-plugin='accelerometer'] p").html(result);
		};

		function onError() {
			navigator.notification.alert("Une erreur est survenue.\n"+error, null, "Erreur");
		};

		watchID_accelerometer = navigator.accelerometer.watchAcceleration(onSuccess, onError, {
			frequency: 300 // 300ms
		});

		$(".result .content div[data-plugin='accelerometer']").fadeIn();
	});

	// Mondialisation
	$(".container button#globalization").click(function() {
		$(".result .content h4").html("Mondialisation");
		$(".result").show();
	});

	// Fermeture du résultat
	$(".result > button").click(function() {
		$(".result").fadeOut(function(){
			$(".result .content div").hide();
			if (watchID_compass) {
				navigator.compass.clearWatch(watchID_compass);
				watchID_compass = null;
			}
			if (watchID_accelerometer) {
				navigator.accelerometer.clearWatch(watchID_accelerometer);
           		watchID_accelerometer = null;
			};
		});
	});
});